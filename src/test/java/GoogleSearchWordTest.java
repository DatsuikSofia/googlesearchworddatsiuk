import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;


public class GoogleSearchWordTest {
    private static Logger LOG = Logger.getLogger(GoogleSearchWordTest.class.getName());
    public static WebDriver driver;


    @BeforeMethod
    public void initializeDriver() {
        {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        }

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");
    }

    @Test
    public static void searchWord() {

        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("Apple");
        searchInput.submit();
        LOG.info("Page title is:" + driver.getTitle() );
        Assert.assertFalse(driver.getTitle().isEmpty());
        ( new WebDriverWait(driver, 20)).until(d -> d.getTitle().toLowerCase().startsWith("apple"));
        LOG.info("Page title is:" + driver.getTitle());
        new  WebDriverWait(driver , 30);
        WebElement imageButton = driver.findElement(By.xpath("//*[@id='hdtb-msb-vis']//div[2]//a"));
        imageButton.click();
        Assert.assertTrue(driver.findElement(By.id("rg")).isDisplayed());
        LOG.info("Image page is opened" + driver.getTitle());

    }

    @AfterMethod
    public void driverTearDown() {
        driver.quit();
    }

}

